# 一、简答题
#### 1、Webpack 的构建流程主要有哪些环节？如果可以请尽可能详尽的描述 Webpack 打包的整个过程。

构建环节：

- 初始化参数

	通过根目录下`webpack.config.js`配置文件或者`--config`命令指定的配置文件拿到项目打包配置信息。

- 开始编译

	通过初始化好的参数创建compiler，并且向compiler挂在所有loader，且挂在文件读写能力，为后续读取与输出准备，此时也会挂在plugins，完毕后执行compoiler.run方法开始编译

- 确定入口

	通过初始化后的参数entry信息（不传递默认src/index.js），确定入口位置。加载入口内容后转换ast语法树，通过对语法树的解析，找出入口文件的依赖模块，并递归此查找，直到最后无依赖

- 模块编译

	在递归查找的过程中，查找到的每个依赖模块根据文件类型调用配置的相应loader进行转换编译，转换完毕后再继续进行依赖模块递归过程。在这个过程中，webpack会抛出不同的勾子，plugin会此时进行处理，将加载与抓换的内容拿到，加上自己的处理后再次返回处理过后内容。如依赖js模块时，通过laoder加载该模块，加载时会通过babel（常见）进行编译处理，转换完毕后会通过ugliify等plugin进行压缩处理。

- 完成编译

	通过模块编译阶段，得到每个模块的最终结果，和模块之间的依赖关系。

- 输出文件

	根据刚才得到的最终结果及依赖关系生成单独chunk文件，此时可以通过plugin对此阶段做最后处理。完毕后将结果输出到outpu配置目录。

　
#### 2、Loader 和 Plugin 有哪些不同？请描述一下开发 Loader 和 Plugin 的思路。

- loader

 webpack默认只能处理js文件，类似于css、font、img等类型文件无法处理。而loader则是提供转换的能力，将不同类型文件转换为webpack可处理的js模块。

 - plugin
	
	通过loader将文件处理为js模块后，对文件的压缩、复制、全局变量、模板等非模块转换的其他所有能力，需要通过plugin进行处理，完成loader无法完成或不便完成的任务。

	plugin是通过监听webpack的声明周期，在不同的生命周期勾子内进行逻辑处理，最终将处理后的内容返回给webpack继续处理。

- 开发loader

	webpack运行在node.js环境中，遵循默认的CommonJs规范，loader是一个遵循CommonJs规范的功能函数。而此功能函数接受的形参则为webpack处理过程中配置的特定类型文件内容（源码），在loader函数内处理该内容（可使用其他三方模块），处理后将最终结果返回以继续执行。

	```js
	// XXX-loader
	module.exports = (source) => {
		const result = source;
		// 此处添加具体逻辑代码

		return result; // 最终返回
	}
	```

- 开发plugin
　
	plugin需是一个函数或者包含apply方法的对象

	```js
	class MyPlugin {
		// 接收配置信息
		constructor(options) {
			// 这里保存和处理
		}

	 	// 接收compiler对象
		// 通过compiler对象挂载和注册相应的勾子回调
		// 这里的注册是通过tapable来注册
		apply(compiler) {
			compiler.hooks.beforeRun.tab('MyPlugin', compiler => {
				// 在此处书写触发时执行的逻辑
				if (compiler.inputFileSystem === inputFileSystem) {
					compiler.fsStartTime = Date.now();
					inputFileSystem.purge();
				}
			})
		}
	}
	```
　

# 二、编程题

#### 1、使用 Webpack 实现 Vue 项目打包任务

> 编程题及文档请看code/vue-app-base目录下内容

> 编程题及文档请看code/vue-app-base目录下内容

> 编程题及文档请看code/vue-app-base目录下内容

具体任务及说明：

1. 在 code/vue-app-base 中安装、创建、编辑相关文件，进而完成作业。
2. 这是一个使用 Vue CLI 创建出来的 Vue 项目基础结构
3. 有所不同的是这里我移除掉了 vue-cli-service（包含 webpack 等工具的黑盒工具）
4. 这里的要求就是直接使用 webpack 以及你所了解的周边工具、Loader、Plugin 还原这个项目的打包任务
5. 尽可能的使用上所有你了解到的功能和特性



**提示：(开始前必看)**

在视频录制后，webpack 版本以迅雷不及掩耳的速度升级到 5，相应 webpack-cli、webpack-dev-server 都有改变。

项目中使用服务器的配置应该是改为下面这样：

```json
// package.json 中部分代码
"scripts": {
	"serve": "webpack serve --config webpack.config.js"
}
```

vue 文件中 使用 style-loader 即可

**其它问题, 可先到 https://www.npmjs.com/ 上搜索查看相应包的最新版本的配置示例, 可以解决大部分问题.**



#### 作业要求

本次作业中的编程题要求大家完成相应代码后

- 提交一个项目说明文档，要求思路流程清晰。
- 或者简单录制一个小视频介绍一下实现思路，并演示一下相关功能。
- 最终将录制的视频或说明文档和代码统一提交至作业仓库。